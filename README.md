# OpenML dataset: COVID-19-biotech-companies-on-stock-exchange(2020)

https://www.openml.org/d/43555

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The coronavirus pandemic has affected the entire world and many families have been destroyed.
The stock exchange was also affected, but vaccine companies took advantage of this moment and leveraged their profits
Biotech companies:

PFIZER: Pfizer Inc. develops, manufactures, and sells healthcare products worldwide. It offers medicines and vaccines in various therapeutic areas.
ASTRAZENECA: Moderna, Inc., a clinical stage biotechnology company, develops therapeutics and vaccines based on messenger RNA for the treatment of infectious diseases, immuno-oncology, rare diseases, and cardiovascular diseases.
BIONTECH: BioNTech SE, a biotechnology company, develops and commercializes immunotherapies for cancer and other infectious diseases.
MODERNA: Moderna, Inc., a clinical stage biotechnology company, develops therapeutics and vaccines based on messenger RNA for the treatment of infectious diseases, immuno-oncology, rare diseases, and cardiovascular diseases.
NOVAVAX: Novavax, Inc., together with its subsidiary, Novavax AB, a late-stage biotechnology company, focuses on the discovery, development, and commercialization of vaccines to prevent serious infectious diseases.

Check the movement of the financial market through this dataset
Use your creativity and external information

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43555) of an [OpenML dataset](https://www.openml.org/d/43555). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43555/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43555/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43555/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

